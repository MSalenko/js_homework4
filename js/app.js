// Теоретичні питання:
//1. ФункціЇ дозволяють зменшити кількість повторювань коду, роблячи його коротшим та функціональнішим;
// 2. Аргумент - це значення з яким функція щось робить та повертає результат. Якщо його не передати він буде undefined;
// 3. Оператор return зупиняє функцію та повертає те що вказано після нього(результат).

// Завдання:

function calc() {
  let firstNumber;
  let secondNumber;
  while (!firstNumber) {
    firstNumber = +prompt('Enter first number');
  }
  while (!secondNumber) {
    secondNumber = +prompt('Enter second number');
  }
  const symbol = prompt('Enter one of operation symbol: + , - , * , / .');
  let result;
  if (symbol === '+') {
    result = firstNumber + secondNumber;
  } else if (symbol === '-') {
    result = firstNumber - secondNumber;
  } else if (symbol === '*') {
    result = firstNumber * secondNumber;
  } else if (symbol === '/') {
    result = firstNumber / secondNumber;
  } else {
    alert('Wrong operation symbol!');
  }
  return result;
}
const result = calc();
if (result !== undefined) {
  console.log(result);
}
